<?php

namespace App\Form;

use App\Entity\Annonce;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use Symfony\Component\Validator\Constraints as Constraints;


class TerrainIndustrielType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder


            ->add('typeAction', ChoiceType::class, [

                'choices' =>
                    ['Acheter' => 'Acheter',
                        'Louer' => 'Louer',

                    ],
                'multiple' =>false,
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'TypeAction...',
                    'class' => 'form-control',
                ],

            ])

            ->add('prix',MoneyType::class, [
                'divisor' => 100,
                'label' => 'Prix(Dt)',
                'attr' => [
                    'placeholder' => 'Prix...',
                    'class' => 'form-control',
                ],
            ])
            ->add('ville',ChoiceType :: class,[

                'choices' =>
                    ['Monastir' => 'Monastir',
                        'Khniss ' => 'Khniss',
                        'Moknine'=>'Moknine',
                        'Kssiba' => 'Kssiba',
                        'Jamel' =>'Jamel',
                        'Omrane' =>'Omrane',
                        'Skanes'=>'Skanes',
                        'Tboulba'=>'Tboulba',
                        'Manzel Hayet'=>'Manzel Hayet',
                        'Manzel Nour' => 'Manzel Nour',

                    ],
                'label' => 'Ville',
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Ville...',
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Regex([
                        'pattern' => "/^[a-zA-Z]+$/",
                        'match' => true,
                        'message' => "Format invalide"]),

                ],

            ])
            ->add('adresse',TextType :: class,[

                'required' => true,
                'label' => 'Adresse',
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Adresse...',
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Regex([
                        'pattern' => "/./",
                        'match' => true,
                        'message' => "Format invalide"]),

                ],

            ])
            ->add('surface',IntegerType::class,[
                'required' => true,
                'label' => 'Surface(m²)',
                'attr' => [

                    'placeholder' => 'Surface...',
                    'class' => 'form-control',
                ],
            ])
            ->add('titreFinancier', choiceType::class, [
                'choices' => ['Oui' => '1', 'Non' => '0'],
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Titre Financier...',
                    'class' => 'form-control',
                ],
            ])
            ->add('mandatExlusive', choiceType::class, [
                'choices' => ['Oui' => '1', 'Non' => '0'],
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Mandat Exlusive...',
                    'class' => 'form-control',
                ],
            ])
            ->add('fraisAgence',IntegerType::class,[
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Frais Agence...',
                    'class' => 'form-control',
                ],
            ])
            ->add('options', ChoiceType::class, [
                    'choices' =>
                        ['Autorisation de bati' => 'Autorisation de bati',
                            'sondage' => 'sondage',
                            'elèctricité'=>'elèctricité',
                            'Clôturée' => 'Clôturée',
                            'bien positionné'=>'bien positionné',

                        ],
                    'multiple' =>true,
                    'attr' => [
                        'autocomplete' => 'off',
                        'placeholder' => 'Option...',
                        'class' => 'form-control',
                    ],
                ]

            )
            ->add('ContactEmail', EmailType::class, [
                'required' => true,
                'trim' => true,
                'label' => 'Contact/Email',
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Email([
                        'checkMX' => true,
                        'message' => "Email invalide"
                    ]),
                ],
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Contact email',
                    'class' => 'form-control',
                ],
            ])
            ->add('ContactTel',TelType::class,[
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Contact Tel',
                    'class' => 'form-control',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Annonce::class,
        ]);
    }
}
