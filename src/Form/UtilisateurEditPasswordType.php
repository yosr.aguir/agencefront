<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UtilisateurEditPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder


            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les champs de mot de passe doivent correspondre',
                'required' => true,
                'first_options' => [
                    'label' => 'Mot de passe',
                    'attr' => [
                        'placeholder' => 'Saisir votre mot de passe',
                        'class' => 'form-control',
                        'data-parsley-trigger' => 'input',
                    ]],
                'second_options' => [
                    'label' => 'Répéter mot de passe',
                    'attr' => [
                        'placeholder' => 'Répéter le mot de passe',
                        'class' => 'form-control',
                        'data-parsley-equalto' => '#register_form_password_first',
                    ]
                ],
            ])

            ->add('AncienPassword' , PasswordType::class,[

                'label' => 'Ancien mot de passe',
                'attr' => [
                    'placeholder' => 'Saisir ancien Password',
                    'class' => 'form-control',

                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
