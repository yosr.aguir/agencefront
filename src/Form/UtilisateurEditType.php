<?php

namespace App\Form;
use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Constraints;


class UtilisateurEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'required' => true,
                'label' => 'Prénom',
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Votre prénom ...',
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Regex([
                        'pattern' => "/^[a-zA-Z]+$/",
                        'match' => true,
                        'message' => "Format invalide"]),
                ],

            ])
            ->add('lastname', TextType::class, [
                'required' => true,
                'label' => 'Nom',
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Votre nom de famille ...',
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Regex([
                        'pattern' => "/^[a-zA-Z]+$/",
                        'match' => true,
                        'message' => "Format invalide"]),
                ],

            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'trim' => true,
                'label' => 'Email',
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Email([
                        'checkMX' => true,
                        'message' => "Email invalide"
                    ]),
                ],
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Votre adresse email',
                    'class' => 'form-control',
                ],
            ])
            ->add('cin', TextType::class, [
                'required' => true,
                'label' => 'CIN',
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Votre CIN',
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Regex([
                        'pattern' => "/^[0-9]{8}$/",
                        'match' => true,
                        'message' => "Format invalide"]),
                ],

            ])
            ->add('datenaissance', DateType::class, [
                'html5' => true,
                'widget' => 'single_text',
                'label' => 'Date de naissance',
            ])
            ->add('adresse', TextType::class, [
                'label' => 'Adresse',
                'required' => true,
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Votre adresse',
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Regex([
                        'pattern' => "/./",
                        'match' => true,
                        'message' => "Format invalide"]),
                ],

            ])


    ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
