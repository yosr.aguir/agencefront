<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message", indexes={@ORM\Index(name="IDX_B6BD307FA76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="envoyer_par", type="string", length=255, nullable=false)
     */
    private $envoyerPar;

    /**
     * @var string
     *
     * @ORM\Column(name="recu_par", type="string", length=255, nullable=false)
     */
    private $recuPar;

    /**
     * @var string
     *
     * @ORM\Column(name="msg", type="text", length=0, nullable=false)
     */
    private $msg;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_time", type="datetime", nullable=false)
     */
    private $dataTime;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Annonce", inversedBy="messages")
     */
    private $idAnnonce;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEnvoyerPar(): ?string
    {
        return $this->envoyerPar;
    }

    public function setEnvoyerPar(string $envoyerPar): self
    {
        $this->envoyerPar = $envoyerPar;

        return $this;
    }

    public function getRecuPar(): ?string
    {
        return $this->recuPar;
    }

    public function setRecuPar(string $recuPar): self
    {
        $this->recuPar = $recuPar;

        return $this;
    }

    public function getMsg(): ?string
    {
        return $this->msg;
    }

    public function setMsg(string $msg): self
    {
        $this->msg = $msg;

        return $this;
    }

    public function getDataTime(): ?\DateTimeInterface
    {
        return $this->dataTime;
    }

    public function setDataTime(\DateTimeInterface $dataTime): self
    {
        $this->dataTime = $dataTime;

        return $this;
    }

    public function getUser(): ?Utilisateur
    {
        return $this->user;
    }

    public function setUser(?Utilisateur $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIdAnnonce(): ?Annonce
    {
        return $this->idAnnonce;
    }

    public function setIdAnnonce(?Annonce $idAnnonce): self
    {
        $this->idAnnonce = $idAnnonce;

        return $this;
    }


}
