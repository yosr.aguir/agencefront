<?php

namespace App\Repository;

use App\Entity\Annonce;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * @method Annonce|null find($id, $lockMode = null, $lockVersion = null)
 * @method Annonce|null findOneBy(array $criteria, array $orderBy = null)
 * @method Annonce[]    findAll()
 * @method Annonce[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnonceRepository extends ServiceEntityRepository
{

    /**
     * Table name
     *
     * @var string
     */
    const tableName = 'annonce';


    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Annonce::class);
    }



    public function getAppartement($action, $ville, $nbrEtage , $surface , $prix)
    {
        $select = [
            'id'=>'a.id',
            'typeAction'=>'a.type_action',
            'nature'=>'a.nature',
            'ville'=>'a.ville',
            'nbrEtage'=>'a.nbr_etage',
            'prix'=>'a.prix',
            'surface'=>'a.surface',
            'imgProfil'=>'a.img_profil',

        ];


        $parameters = [
            ':action' => $action,
            ':ville' => $ville,
            ':etage' => $nbrEtage,
            ':surface'=>$surface,
            ':prix'=>$prix,
        ];

        $sql = '';
        $rsm = new ResultSetMapping();

        foreach ($select as $column => $value) {
            $sql .= $value.' AS '.$column.', ';
            $rsm->addScalarResult($column, $column);
        }

        $sql = 'SELECT '.substr($sql, 0, -2).' FROM '.self::tableName.' AS a '
            ." WHERE  a.nature = 'Appartement' ";
        if ($nbrEtage != "Nombre d'etages") {
            if ($nbrEtage != "6+"){
                $sql .=  "AND a.nbr_etage = :etage ";
            } else {
                $sql .=  "AND a.nbr_etage >= 6 ";
            }
        }

        if ($ville != "Ville") {
            $sql .=  "AND a.ville = :ville ";
        }
        if ($action != "Type d'action") {
            $sql .=  "AND a.type_action = :action ";
        }

        if ($surface != "Surface") {
            if ($surface != "500+") {
                $surface = str_replace("-", " AND ", $surface);
                $sql .= "AND a.surface BETWEEN $surface ";
            }else{
                $sql .= "AND a.surface >= 500 ";

            }
        }
        if ($prix != "Prix") {
            if( $prix != "10000+") {
                $prix = str_replace("-", " AND ", $prix);
                $sql .= "AND a.prix BETWEEN $prix ";
            }else{
                $sql .= "AND a.prix >= 1000 ";

            }


        }


        $cacheKey = sha1($sql.json_encode($parameters));
        $result = $this
            ->getEntityManager()
            ->createNativeQuery($sql, $rsm)
            ->setParameters($parameters)
            ->getScalarResult();

        if (count($result)) {
            return $result;
        }

        return [];
    }
    public function getMaison($action, $ville, $nbrPieces , $surface , $prix)
    {
        $select = [
            'id'=>'a.id',
            'typeAction'=>'a.type_action',
            'nature'=>'a.nature',
            'ville'=>'a.ville',
            'nbrPieces'=>'a.nbr_pieces',
            'prix'=>'a.prix',
            'surface'=>'a.surface',
            'imgProfil'=>'a.img_profil',


        ];


        $parameters = [
            ':action' => $action,
            ':ville' => $ville,
            ':pieces' => $nbrPieces,
            ':surface'=>$surface,
            ':prix'=>$prix,
        ];

        $sql = '';
        $rsm = new ResultSetMapping();

        foreach ($select as $column => $value) {
            $sql .= $value.' AS '.$column.', ';
            $rsm->addScalarResult($column, $column);
        }

        $sql = 'SELECT '.substr($sql, 0, -2).' FROM '.self::tableName.' AS a '
            ." WHERE  a.nature = 'Maison' ";
        if ($nbrPieces != "Nombre des piéces") {
            if ($nbrPieces != "6+"){
                $sql .=  "AND a.nbr_pieces = :pieces ";
            } else {
                $sql .=  "AND a.nbr_pieces >= 6 ";
            }

        }
        if ($ville != "Ville") {
            $sql .=  "AND a.ville = :ville ";
        }
        if ($action != "Type d'action") {
            $sql .=  "AND a.type_action = :action ";
        }
        if ($surface != "Surface") {
            if ($surface != "500+") {
                $surface = str_replace("-", " AND ", $surface);
                $sql .= "AND a.surface BETWEEN $surface ";
            }else{
                $sql .= "AND a.surface >= 500 ";

            }
        }
        if ($prix != "Prix") {
            if( $prix != "10000+") {
                $prix = str_replace("-", " AND ", $prix);
                $sql .= "AND a.prix BETWEEN $prix ";
            }else{
                $sql .= "AND a.prix >= 1000 ";

            }
        }


        $cacheKey = sha1($sql.json_encode($parameters));

        $result = $this
            ->getEntityManager()
            ->createNativeQuery($sql, $rsm)
            ->setParameters($parameters)
            ->getScalarResult();

        if (count($result)) {
            return $result;
        }

        return [];
    }
    public function getVilla($action, $ville, $nbrPieces , $surface , $prix)
    {
        $select = [
            'id'=>'a.id',
            'typeAction'=>'a.type_action',
            'nature'=>'a.nature',
            'ville'=>'a.ville',
            'nbrPieces'=>'a.nbr_pieces',
            'prix'=>'a.prix',
            'surface'=>'a.surface',
            'imgProfil'=>'a.img_profil',

        ];


        $parameters = [
            ':action' => $action,
            ':ville' => $ville,
            ':pieces' => $nbrPieces,
            ':surface'=>$surface,
            ':prix'=>$prix,
        ];

        $sql = '';
        $rsm = new ResultSetMapping();

        foreach ($select as $column => $value) {
            $sql .= $value.' AS '.$column.', ';
            $rsm->addScalarResult($column, $column);
        }

        $sql = 'SELECT '.substr($sql, 0, -2).' FROM '.self::tableName.' AS a '
            ." WHERE  a.nature = 'Villa' ";
        if ($nbrPieces != "Nombre des piéces") {
            if ($nbrPieces != "6+"){
                $sql .=  "AND a.nbr_pieces = :pieces ";
            } else {
                $sql .=  "AND a.nbr_pieces >= 6 ";
            }

        }
        if ($ville != "Ville") {
            $sql .=  "AND a.ville = :ville ";
        }
        if ($action != "Type d'action") {
            $sql .=  "AND a.type_action = :action ";
        }
        if ($surface != "Surface") {
            if ($surface != "500+") {
                $surface = str_replace("-", " AND ", $surface);
                $sql .= "AND a.surface BETWEEN $surface ";
            } else {
                $sql .= "AND a.surface >= 500 ";

            }
        }
        if ($prix != "Prix") {
            if( $prix != "10000+") {
                $prix = str_replace("-", " AND ", $prix);
                $sql .= "AND a.prix BETWEEN $prix ";
            }else{
                $sql .= "AND a.prix >= 1000 ";

            }
        }


        $cacheKey = sha1($sql.json_encode($parameters));

        $result = $this
            ->getEntityManager()
            ->createNativeQuery($sql, $rsm)
            ->setParameters($parameters)
            ->getScalarResult();

        if (count($result)) {
            return $result;
        }

        return [];
    }
    public function getAgricole($action, $ville , $surface , $prix)
    {
        $select = [
            'id'=>'a.id',
            'typeAction'=>'a.type_action',
            'nature'=>'a.nature',
            'ville'=>'a.ville',
            'prix'=>'a.prix',
            'surface'=>'a.surface',
            'imgProfil'=>'a.img_profil',


        ];


        $parameters = [
            ':action' => $action,
            ':ville' => $ville,
            ':surface'=>$surface,
            ':prix'=>$prix,
        ];

        $sql = '';
        $rsm = new ResultSetMapping();

        foreach ($select as $column => $value) {
            $sql .= $value.' AS '.$column.', ';
            $rsm->addScalarResult($column, $column);
        }

        $sql = 'SELECT '.substr($sql, 0, -2).' FROM '.self::tableName.' AS a '
            ." WHERE  a.nature = 'Terrain Agricole' ";

        if ($ville != "Ville") {
            $sql .=  "AND a.ville = :ville ";
        }
        if ($action != "Type d'action") {
            $sql .=  "AND a.type_action = :action ";
        }
        if ($surface != "Surface") {
            if ($surface != "500+") {
                $surface = str_replace("-", " AND ", $surface);
                $sql .= "AND a.surface BETWEEN $surface ";
            } else {
                $sql .= "AND a.surface >= 500 ";

            }
        }
        if ($prix != "Prix") {
            if( $prix != "10000+") {
                $prix = str_replace("-", " AND ", $prix);
                $sql .= "AND a.prix BETWEEN $prix ";
            }else{
                $sql .= "AND a.prix >= 1000 ";

            }
        }


        $cacheKey = sha1($sql.json_encode($parameters));

        $result = $this
            ->getEntityManager()
            ->createNativeQuery($sql, $rsm)
            ->setParameters($parameters)
            ->getScalarResult();

        if (count($result)) {
            return $result;
        }

        return [];
    }
    public function getConstructible($action, $ville , $surface , $prix)
    {
        $select = [
            'id'=>'a.id',
            'typeAction'=>'a.type_action',
            'nature'=>'a.nature',
            'ville'=>'a.ville',
            'prix'=>'a.prix',
            'surface'=>'a.surface',
            'imgProfil'=>'a.img_profil',


        ];


        $parameters = [
            ':action' => $action,
            ':ville' => $ville,
            ':surface'=>$surface,
            ':prix'=>$prix,
        ];

        $sql = '';
        $rsm = new ResultSetMapping();

        foreach ($select as $column => $value) {
            $sql .= $value.' AS '.$column.', ';
            $rsm->addScalarResult($column, $column);
        }

        $sql = 'SELECT '.substr($sql, 0, -2).' FROM '.self::tableName.' AS a '
            ." WHERE  a.nature = 'Terrain Constructible' ";

        if ($ville != "Ville") {
            $sql .=  "AND a.ville = :ville ";
        }
        if ($action != "Type d'action") {
            $sql .=  "AND a.type_action = :action ";
        }
        if ($surface != "Surface") {
            if ($surface != "500+") {
                $surface = str_replace("-", " AND ", $surface);
                $sql .= "AND a.surface BETWEEN $surface ";
            } else {
                $sql .= "AND a.surface >= 500 ";

            }
        }
        if ($prix != "Prix") {
            if( $prix != "10000+") {
                $prix = str_replace("-", " AND ", $prix);
                $sql .= "AND a.prix BETWEEN $prix ";
            }else{
                $sql .= "AND a.prix >= 1000 ";

            }
        }


        $cacheKey = sha1($sql.json_encode($parameters));

        $result = $this
            ->getEntityManager()
            ->createNativeQuery($sql, $rsm)
            ->setParameters($parameters)
            ->getScalarResult();

        if (count($result)) {
            return $result;
        }

        return [];
    }
    public function getIndustriel($action, $ville , $surface , $prix)
    {
        $select = [
            'id'=>'a.id',
            'typeAction'=>'a.type_action',
            'nature'=>'a.nature',
            'ville'=>'a.ville',
            'prix'=>'a.prix',
            'surface'=>'a.surface',
            'imgProfil'=>'a.img_profil',


        ];


        $parameters = [
            ':action' => $action,
            ':ville' => $ville,
            ':surface'=>$surface,
            ':prix'=>$prix,
        ];

        $sql = '';
        $rsm = new ResultSetMapping();

        foreach ($select as $column => $value) {
            $sql .= $value.' AS '.$column.', ';
            $rsm->addScalarResult($column, $column);
        }

        $sql = 'SELECT '.substr($sql, 0, -2).' FROM '.self::tableName.' AS a '
            ." WHERE  a.nature = 'Terrain Industriel' ";

        if ($ville != "Ville") {
            $sql .=  "AND a.ville = :ville ";
        }
        if ($action != "Type d'action") {
            $sql .=  "AND a.type_action = :action ";
        }
        if ($surface != "Surface") {
            if ($surface != "500+") {
                $surface = str_replace("-", " AND ", $surface);
                $sql .= "AND a.surface BETWEEN $surface ";
            } else {
                $sql .= "AND a.surface >= 500 ";

            }
        }
        if ($prix != "Prix") {
            if( $prix != "10000+") {
                $prix = str_replace("-", " AND ", $prix);
                $sql .= "AND a.prix BETWEEN $prix ";
            }else{
                $sql .= "AND a.prix >= 1000 ";

            }
        }


        $cacheKey = sha1($sql.json_encode($parameters));

        $result = $this
            ->getEntityManager()
            ->createNativeQuery($sql, $rsm)
            ->setParameters($parameters)
            ->getScalarResult();

        if (count($result)) {
            return $result;
        }

        return [];
    }
    public function getLocal($action, $ville, $nbrPieces , $surface , $prix)
    {
        $select = [
            'id'=>'a.id',
            'typeAction'=>'a.type_action',
            'nature'=>'a.nature',
            'ville'=>'a.ville',
            'nbrPieces'=>'a.nbr_pieces',
            'prix'=>'a.prix',
            'surface'=>'a.surface',
            'imgProfil'=>'a.img_profil',


        ];


        $parameters = [
            ':action' => $action,
            ':ville' => $ville,
            ':pieces' => $nbrPieces,
            ':surface'=>$surface,
            ':prix'=>$prix,
        ];

        $sql = '';
        $rsm = new ResultSetMapping();

        foreach ($select as $column => $value) {
            $sql .= $value.' AS '.$column.', ';
            $rsm->addScalarResult($column, $column);
        }

        $sql = 'SELECT '.substr($sql, 0, -2).' FROM '.self::tableName.' AS a '
            ." WHERE  a.nature = 'Local Commercial' ";
        if ($nbrPieces != "Nombre des piéces") {
            if ($nbrPieces != "6+"){
                $sql .=  "AND a.nbr_pieces = :pieces ";
            } else {
                $sql .=  "AND a.nbr_pieces >= 6 ";
            }

        }
        if ($ville != "Ville") {
            $sql .=  "AND a.ville = :ville ";
        }
        if ($action != "Type d'action") {
            $sql .=  "AND a.type_action = :action ";
        }
        if ($surface != "Surface") {
            if ($surface != "500+") {
                $surface = str_replace("-", " AND ", $surface);
                $sql .= "AND a.surface BETWEEN $surface ";
            } else {
                $sql .= "AND a.surface >= 500 ";

            }
        }
        if ($prix != "Prix") {
            if( $prix != "10000+") {
                $prix = str_replace("-", " AND ", $prix);
                $sql .= "AND a.prix BETWEEN $prix ";
            }else{
                $sql .= "AND a.prix >= 1000 ";

            }
        }


        $cacheKey = sha1($sql.json_encode($parameters));

        $result = $this
            ->getEntityManager()
            ->createNativeQuery($sql, $rsm)
            ->setParameters($parameters)
            ->getScalarResult();

        if (count($result)) {
            return $result;
        }

        return [];
    }
    public function getStudio($action, $ville , $surface , $prix)
    {
        $select = [
            'id'=>'a.id',
            'typeAction'=>'a.type_action',
            'nature'=>'a.nature',
            'ville'=>'a.ville',
            'prix'=>'a.prix',
            'surface'=>'a.surface',
            'imgProfil'=>'a.img_profil',


        ];


        $parameters = [
            ':action' => $action,
            ':ville' => $ville,
            ':surface'=>$surface,
            ':prix'=>$prix,
        ];

        $sql = '';
        $rsm = new ResultSetMapping();

        foreach ($select as $column => $value) {
            $sql .= $value.' AS '.$column.', ';
            $rsm->addScalarResult($column, $column);
        }

        $sql = 'SELECT '.substr($sql, 0, -2).' FROM '.self::tableName.' AS a '
            ." WHERE  a.nature = 'Studio' ";

        if ($ville != "Ville") {
            $sql .=  "AND a.ville = :ville ";
        }
        if ($action != "Type d'action") {
            $sql .=  "AND a.type_action = :action ";
        }
        if ($surface != "Surface") {
            if ($surface != "500+") {
                $surface = str_replace("-", " AND ", $surface);
                $sql .= "AND a.surface BETWEEN $surface ";
            } else {
                $sql .= "AND a.surface >= 500 ";

            }
        }
        if ($prix != "Prix") {
            if( $prix != "10000+") {
                $prix = str_replace("-", " AND ", $prix);
                $sql .= "AND a.prix BETWEEN $prix ";
            }else{
                $sql .= "AND a.prix >= 1000 ";

            }
        }


        $cacheKey = sha1($sql.json_encode($parameters));

        $result = $this
            ->getEntityManager()
            ->createNativeQuery($sql, $rsm)
            ->setParameters($parameters)
            ->getScalarResult();

        if (count($result)) {
            return $result;
        }

        return [];
    }

    public function getAnnonce()
    {
        $select = [
            'id'=>'a.id',
            'typeAction'=>'a.type_action',
            'nature'=>'a.nature',
            'ville'=>'a.ville',
            'prix'=>'a.prix',
            'surface'=>'a.surface',
            'DateAdd'=>'a.date_add',
            'imgProfil'=>'a.img_profil',


        ];



        $sql = '';
        $rsm = new ResultSetMapping();

        foreach ($select as $column => $value) {
            $sql .= $value.' AS '.$column.', ';
            $rsm->addScalarResult($column, $column);
        }

        $sql = 'SELECT '.substr($sql, 0, -2).' FROM '.self::tableName.' AS a '
            ." ORDER BY a.date_add DESC"
            ." LIMIT 6 ";


        //$cacheKey = sha1($sql.json_encode($parameters));

        $result = $this
            ->getEntityManager()
            ->createNativeQuery($sql, $rsm)
            //->setParameters($parameters)
            ->getScalarResult();

        if (count($result)) {
            return $result;
        }

        return [];
    }




    // /**
    //  * @return Annonce[] Returns an array of Annonce objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Annonce
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
