<?php

namespace App\Repository;

use App\Entity\Map;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Map|null find($id, $lockMode = null, $lockVersion = null)
 * @method Map|null findOneBy(array $criteria, array $orderBy = null)
 * @method Map[]    findAll()
 * @method Map[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MapRepository extends ServiceEntityRepository
{
    /**
     * Table name
     *
     * @var string
     */
    const tableName = 'map';

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Map::class);
    }


    public function findOneByAnnonce($annonce)
    {
        $select = [
            'id'=>'m.id',
            'latitude'=>'m.latitude',
            'longitude'=>'m.longitude',
            'prix'=>'a.prix',
            'idAnnonce'=>'m.id_annonce_id',

        ];


        $parameters = [
            ':idAnnonce' => $annonce,
        ];

        $sql = '';
        $rsm = new ResultSetMapping();

        foreach ($select as $column => $value) {
            $sql .= $value.' AS '.$column.', ';
            $rsm->addScalarResult($column, $column);
        }

        $sql = 'SELECT '.substr($sql, 0, -2).' FROM '.self::tableName.' AS m '.
                'LEFT JOIN annonce as a ON m.id_annonce_id = a.id WHERE m.id_annonce_id = :idAnnonce';


        $cacheKey = sha1($sql.json_encode($parameters));

        $result = $this
            ->getEntityManager()
            ->createNativeQuery($sql, $rsm)
            ->setParameters($parameters)
            ->getScalarResult();

        if (count($result)) {
            return $result;
        }

        return [];
    }











    // /**
    //  * @return Map[] Returns an array of Map objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Map
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}
