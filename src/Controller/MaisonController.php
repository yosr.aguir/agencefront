<?php

namespace App\Controller;
use App\Form\MaisonType;
use App\Repository\AvisRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\Avis;
use App\Entity\Message;
use App\Entity\Images;
use App\Repository\ImagesRepository;
use App\Repository\AnnonceRepository;
use App\Repository\MessageRepository;



class MaisonController extends AbstractController
{
    /**
     * @Route("/maison", name="maison")
     */
    public function index()
    {
        return $this->render('maison/index.html.twig', [
            'controller_name' => 'MaisonController',
        ]);
    }

    /**
     * @Route("/vente/maison", name="maison_vente")
     *    Method({"GET" , "POST"})
     */
    public function vente(AnnonceRepository $repository)
    {
        $maisonVente = $repository->findBy(['nature' => 'Maison', 'typeAction' => 'Acheter']);
        return $this->render('accueil/vente_maison.html.twig', [
            'maison' => $maisonVente,
        ]);
    }


    /**
     * @Route("/maison/edit/{id}" , name="maison_edit",requirements={"id":"\d+"})
     * Method({"GET" , "POST"})
     */
    public function edit(Request $request, Annonce $annonce, ImagesRepository $imagesRepository)
    {
        $images = $imagesRepository->findBy(['idAnnonce' => $annonce]);
        $form = $this->createForm(MaisonType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted()){
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if ($_FILES['picProfilMaison']['name'] !== '') {
                    $path = $_FILES['picProfilMaison']['name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picProfilLocal']['tmp_name']);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $annonce->setImgProfil($base64);
                }
                $em->persist($annonce);
                $em->flush();

                for ($i=0; $i<sizeof($_FILES['picMaison']['name']); $i++) {
                    $path = $_FILES['picMaison']['name'][$i];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picCommercial']['tmp_name'][$i]);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $image = new Images();
                    $image->setImg($base64)
                        ->setIdAnnonce($annonce);

                    $em->persist($image);
                    $em->flush();
                    $this->addFlash(
                        'info',
                        'Annonce Bien Modifiée'
                    );
                }

                return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));
            } else {
                return $this->render('maison/edit.html.twig', array(
                    'annonce' => $annonce,
                    'form' => $form->createView(),
                    'images' => $images
                ));
            }
        }

        return $this->render('maison/edit.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
            'images' => $images
        ));
    }


    /**
     * @Route("maison/{id}", name="maison_show", methods={"GET"},requirements={"id":"\d+"})
     */
    public function show(Annonce $annonce ,ImagesRepository $imagesRepository): Response
    {
        $images = $imagesRepository->findby(['idAnnonce' =>$annonce]);
        return $this->render('maison/show.html.twig', [
            'annonce' => $annonce,
            'id' => 1,
            'images'=>$images,
        ]);
    }
    /**
     * @Route("/afficher/maison/{id}", name="maison_afficher_one" )
     */
    public function afficherOne(Annonce $annonce, ImagesRepository $repository , AvisRepository $avisRepository)
    {

        $avis =$avisRepository->findBy(['idAnnonce'=>$annonce]);
        $images = $repository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('maison/afficherVente.html.twig', [
            'maisons' =>  $annonce,
            'images' =>  $images,
            'avis'=> $avis ,

        ]);
    }

    /**
     * @Route("/message/envoyer/maison/{action}/{id}", name="send_msg_maison")
     */
    public function sendMessage($action,Annonce $annonce, Request $request, MessageRepository $messageRepository, ImagesRepository $repository, AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce' => $annonce]);
        $avis =$avisRepository->findBy(['idAnnonce'=>$annonce]);
        $msg = new Message();
        $msg->setMsg($request->request->get('message'))
            ->setDataTime(new \DateTime())
            ->setEnvoyerPar("User")
            ->setUser($this->getUser())
            ->setRecuPar("Admin")
            ->setIdAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($msg);
        $this->getDoctrine()->getManager()->flush();
       //dump($messageRepository->findAll());exit;

        if ($action == "acheter") {

            return $this->render('maison/afficherVente.html.twig', [
                'images' => $images,
                'maisons' => $annonce,
                'message' => 'message envoyé',
                'avis'=> $avis ,
            ]);
        }

    }

    /**
     * @Route("/commenter/maison/{id}", name="commenter_maison")
     */
    public function CommenterMaison(Annonce $annonce, Request $request)
    {
        //dump($request->request->all());exit;

        $avis = new Avis();
        $avis->setCommentaire($request->request->get('commentaire'))
            ->setdate(new\DateTime())
            ->setIduser($this->getUser())
            ->setidAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($avis);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('maison_afficher_one',['id'=>$annonce->getId()]);
    }

    }
