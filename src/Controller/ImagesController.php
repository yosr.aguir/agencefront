<?php

namespace App\Controller;
use App\Entity\Annonce;
use App\Entity\Images;
use App\Form\ImagesType;
use App\Repository\ImagesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/images")
 */
class ImagesController extends AbstractController
{
    /**
     * @Route("/", name="images_index", methods={"GET"})
     */
    public function index(ImagesRepository $imagesRepository): Response
    {
        return $this->render('images/index.html.twig', [
            'images' => $imagesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="images_new", methods={"GET","POST"})
     */
    public function new(Request $request ,Images $image): Response
    {
        $image = new Images();
        $form = $this->createForm(ImagesType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($image);
            $entityManager->flush();

            return $this->redirectToRoute('images_index');
        }

        return $this->render('images/new.html.twig', [
            'image' => $image,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="images_show", methods={"GET"})
     */
    public function show(Images $image): Response
    {
        return $this->render('images/show.html.twig', [
            'image' => $image,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="images_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Images $image): Response
    {
        $form = $this->createForm(ImagesType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('images_index', [
                'id' => $image->getId(),
            ]);
        }

        return $this->render('images/edit.html.twig', [
            'image' => $image,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="images_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Images $image): Response
    {
        if ($this->isCsrfTokenValid('delete' . $image->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($image);
            $entityManager->flush();
        }

        return $this->redirectToRoute('images_index');
    }


    /**
     * @Route("/{id}/{idAnnonce}/{idForm}", name="supprimer_photo")
     */
    public function deleteImg(Request $request, Images $image, $idAnnonce, $idForm): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($image);
        $entityManager->flush();

        switch ($idForm) {
            case 0:
                return $this->redirectToRoute('appartement_edit', ['id' => $idAnnonce]);
                break;
            case 1:
                return $this->redirectToRoute('maison_edit', ['id' => $idAnnonce]);
                break;
            case 2 ;
                return $this->redirectToRoute('villa_edit', ['id' => $idAnnonce]);
                break;
            case 3:
                return $this->redirectToRoute('edit_terrain_agricole', ['id' => $idAnnonce]);
                break;
            case 4:
                return $this->redirectToRoute('edit_terrain_industreil', ['id' => $idAnnonce]);
                break;
            case 5:
                return $this->redirectToRoute('edit_terrain_constructible', ['id' => $idAnnonce]);
                break;
            case 6:
                return $this->redirectToRoute('local_commercial_edit', ['id' => $idAnnonce]);
                break;
            case 7:
                return $this->redirectToRoute('studio_edit', ['id' => $idAnnonce]);
                break;
        }
    }
}
