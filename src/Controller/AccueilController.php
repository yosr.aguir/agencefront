<?php

namespace App\Controller;

Use App\Repository\AnnonceRepository;
use App\Repository\ImagesRepository;
use App\Repository\MessageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;


class AccueilController extends AbstractController
{


    public function getAnnonce(AnnonceRepository $repository){

        $result = $repository->getAnnonce();
        //dump($result);exit;
        return $this->render('accueil/getAnnonce.html.twig',[
            'result' => $result,
        ]);



    }

}
