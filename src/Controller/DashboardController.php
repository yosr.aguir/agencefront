<?php

namespace App\Controller;

use App\Repository\AnnonceRepository;
use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="dashboard")
     */
    public function index()
    {
        return $this->render('accueil/index.html.twig');
    }

    /**
     * @Route("/dashboard", name="dashboard_user")
     */
    public function dashboardUser( AnnonceRepository $annonceRepository)
    {
        $maisons =$annonceRepository->findBy(['idUserId' => $this->getUser() ,'nature' => 'Maison']);
        $appartements =$annonceRepository->findBy(['idUserId' => $this->getUser() ,'nature' => 'Appartement']);
        $villas =$annonceRepository->findBy(['idUserId' => $this->getUser() ,'nature' => 'Villa']);
        $studios =$annonceRepository->findBy(['idUserId' => $this->getUser() ,'nature' => 'Studio']);
        $local =$annonceRepository->findBy(['idUserId' => $this->getUser() ,'nature' => 'Local Commercial']);
        $constructible =$annonceRepository->findBy(['idUserId' => $this->getUser() ,'nature' => 'constructible']);
        $industriel =$annonceRepository->findBy(['idUserId' => $this->getUser() ,'nature' => 'industriel']);
        $agricole =$annonceRepository->findBy(['idUserId' => $this->getUser() ,'nature' => 'agricole']);
        $nbrmaison = count($maisons);
        $nbrappartement = count($appartements);
        $nbrvilla = count($villas);
        $nbrstudio = count($studios);
        $nbrlocal = count($local);
        $nbrconstructible = count($constructible);
        $nbrindustriel = count($industriel);
        $nbragricole = count($agricole);
        return $this->render('dashboard/dashboard.html.twig',[
            'nbrannonce'=>$nbrappartement,
            'nbrmaison'=>$nbrmaison,
            'nbrvilla'=>$nbrvilla,
            'nbrstudio'=>$nbrstudio,
            'nbrlocal'=>$nbrlocal,
            'nbrconstructibLe'=>$nbrconstructible,
            'nbrindustriel'=>$nbrindustriel,
            'nbragricole'=>$nbragricole,


        ]);
    }
}
