<?php

namespace App\Controller;

use App\Entity\Message;
use App\Repository\MessageRepository;
use App\Entity\Notification;
use App\Repository\NotificationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MessageController extends AbstractController
{
    /**
     * @Route("/message", name="message")
     */
    public function index( MessageRepository $repository)
    {
        $msg = $repository ->findBy(['user'=> $this->getUser()->getId()]);
        return $this->render('message/message.html.twig', [
          'msgs' => $msg,
        ]);
    }

}
