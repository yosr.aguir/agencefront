<?php

namespace App\Controller;

use App\Entity\Notification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\NotificationRepository;

class NotificationController extends AbstractController
{
    /**
     * @Route("/notification", name="notification")
     */

    public function index()
{
    return $this->render('notification/index1.html.twig', [
        'controller_name' => 'NotificationController',
    ]);
}


    public function message(NotificationRepository $repository)
    {
        $all = $repository->findBy(['vu'=>0]);
        $notification = [];
        foreach ( $all as $n){
            $ch = $n->getNotification();
            $msg = substr($ch, 0, strpos($ch, ':') - 1);

            if ($msg == "Vous avez recu un nouveau message"){
                if (substr($ch, strpos($ch, ':') + 1, strlen($msg))=='Admin'){
                    array_push($notification,["msg"=>$msg]);
                }
            }
        }
        return $this->render('notification/index1.html.twig', [
            'messages'=>$notification,
            'nbr'=>count($notification)
        ]);
    }

    /**
     * @Route("/message/vu" , name="message_vu")
     *
     */
    public function vu(NotificationRepository $repository)
    {
        $em = $this->getDoctrine()->getManager();

        $notifications = $repository->findBy(['vu'=>0]);
        foreach ( $notifications as $n){
            $ch = $n->getNotification();
            $msg = substr($ch, 0, strpos($ch, ':') - 1);

            if ($msg == "Vous avez recu un nouveau message"){
                if (substr($ch, strpos($ch, ':') + 1, strlen($msg))=='Admin'){
                   $n->setVu(1);
                    $em->persist($n);
                    $em->flush();
                }
            }
        }

        return $this->redirectToRoute('message');
    }
}
