<?php

namespace App\Controller;
use App\Form\VillaType;
use App\Repository\AvisRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\Images;
use App\Repository\ImagesRepository;
use App\Repository\AnnonceRepository;
use App\Repository\MessageRepository;

class VillaController extends AbstractController
{
    /**
     * @Route("/villa", name="villa")
     */
    public function index()
    {
        return $this->render('villa/index.html.twig', [
            'controller_name' => 'VillaController',
        ]);
    }

    /**
     * @Route("/vente/villa", name="villa_vente")
     *    Method({"GET" , "POST"})
     */
    public function vente(AnnonceRepository $repository)
    {
        $villaVente = $repository->findBy(['nature'=>'Villa' ,'typeAction'=>'Acheter']);
        return $this->render('accueil/vente_villa.html.twig', [
            'villa' => $villaVente,
        ]);
    }
    /**
     * @Route("/location/villa", name="villa_location")
     */
    public function location(AnnonceRepository  $repository)
    {
        $villaLocation = $repository->findBy(['nature'=>'villa' , 'typeAction'=>'Louer']);
        return $this->render('accueil/location_villa.html.twig', [
            'villa' => $villaLocation,
        ]);
    }
    /**
     * @Route("/villa/edit/{id}" , name="villa_edit",requirements={"id":"\d+"})
     * Method({"GET" , "POST"})
     */
    public function edit(Request $request, Annonce $annonce , ImagesRepository $imagesRepository)
    {

        $images = $imagesRepository->findby(['idAnnonce' =>$annonce]);
        $form = $this->createForm(VillaType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() ){
            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if ($_FILES['picProfilVilla']['name'] !== '') {
                    $path = $_FILES['picProfilVilla']['name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picProfilVilla']['tmp_name']);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $annonce->setImgProfil($base64);
                }
            $em->persist($annonce);
            $em->flush();

            for ($i=0; $i<sizeof($_FILES['picVilla']['name']); $i++){
                $path = $_FILES['picVilla']['name'][$i];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picVilla']['tmp_name'][$i]);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $image = new Images();
                $image->setImg($base64)
                    ->setIdAnnonce($annonce);


                $em->persist($image);
                $em->flush();
                $this->addFlash(
                    'info',
                    'Annonce Bien Modifiée'
                );
            }
            return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));
        }else {
                return $this->render('villa/edit.html.twig', array(
                    'annonce' => $annonce,
                    'form' => $form->createView(),
                    'images'=>$images,
                ));
            }
        }
        return $this->render('villa/edit.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
            'images'=>$images,
        ));
    }

    /**
     * @Route("villa/{id}", name="villa_show", methods={"GET"},requirements={"id":"\d+"})
     */
    public function show(Annonce $annonce , ImagesRepository $imagesRepository): Response
    {
        $images = $imagesRepository->findby(['idAnnonce' =>$annonce]);
        return $this->render('villa/show.html.twig', [
            'annonce' => $annonce,
            'id' => 2,
            'images'=>$images ,
        ]);
    }

    /**
     * @Route("/location/villa/{id}", name="villa_afficher_location" )
     */
    public function afficherLocation(Annonce $annonce, ImagesRepository $repository , AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce'=>$annonce]);
        $avis = $avisRepository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('villa/afficherLocation.html.twig', [
            'villas' =>  $annonce,
            'images' =>  $images,
            'avis' => $avis,
        ]);
    }

    /**
     * @Route("/afficher/villa/{id}", name="villa_afficher_one" )
     */
    public function afficherOne(Annonce $annonce, ImagesRepository $repository , AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce'=>$annonce]);
        $avis = $avisRepository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('villa/afficherVente.html.twig', [
            'villas' =>  $annonce,
            'images' =>  $images,
            'avis' => $avis,

        ]);
    }
    /**
     * @Route("/message/envoyer/villa/{action}/{id}", name="send_msg_villa")
     */
    public function sendMessage($action,Annonce $annonce, Request $request, MessageRepository $messageRepository, ImagesRepository $repository , AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce' => $annonce]);
        $avis = $annonce->findBy(['idAnnonce' => $annonce]);
        $msg = new Message();
        $msg->setMsg($request->request->get('message'))
            ->setDataTime(new \DateTime())
            ->setEnvoyerPar("User")
            ->setUser($this->getUser())
            ->setRecuPar("Admin")
            ->setIdAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($msg);
        $this->getDoctrine()->getManager()->flush();
        //dump($messageRepository->findAll());exit;

        if ($action == "acheter") {

            return $this->render('villa/afficherVente.html.twig', [
                'images' => $images,
                'villas' => $annonce,
                'avis'=> $avis,
                'message'=>'message envoyé',
            ]);
        } else {
            return $this->render('villa/afficherLocation.html.twig', [
                'images' => $images,
                'villas' => $annonce,
                'avis'=> $avis,
                'message'=>'message envoyé',
            ]);
        }

    }
    /**
     * @Route("/commenter/villa/{id}", name="commenter_villa")
     */
    public function CommenterMaison(Annonce $annonce, Request $request)
    {
        //dump($request->request->all());exit;

        $avis = new Avis();
        $avis->setCommentaire($request->request->get('commentaire'))
            ->setdate(new\DateTime())
            ->setIduser($this->getUser())
            ->setidAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($avis);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('villa_afficher_one',['id'=>$annonce->getId()]);
    }
}
