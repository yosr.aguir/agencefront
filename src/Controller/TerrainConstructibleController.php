<?php

namespace App\Controller;
use App\Form\TerrainConstructibleType;
use App\Repository\AvisRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\Images;
use App\Repository\ImagesRepository;
use App\Repository\AnnonceRepository;
use App\Repository\MessageRepository;


class TerrainConstructibleController extends AbstractController
{
    /**
     * @Route("/terrain/constructible", name="terrain_constructible")
     */
    public function index()
    {
        return $this->render('terrain_constructible/index.html.twig', [
            'controller_name' => 'TerrainConstructibleController',
        ]);
    }
    /**
     * @Route("/vente/terrainConstructible", name="terrain_constructible_vente")
     *    Method({"GET" , "POST"})
     */
    public function vente(AnnonceRepository $repository)
    {
        $terrainConstructibleVente = $repository->findBy(['nature'=>'Terrain Constructible' ,'typeAction'=>'Acheter']);
        return $this->render('accueil/vente_terrain_constructible.html.twig', [
            'terrainConstructible' => $terrainConstructibleVente,
        ]);
    }
    /**
     * @Route("/terrainConstrutible/edit/{id}" , name="edit_terrain_constructible",requirements={"id":"\d+"})
     * Method({"GET" , "POST"})
     */
    public function edit(Request $request, Annonce $annonce , ImagesRepository $imagesRepository )
    {


        $images = $imagesRepository->findBy(['idAnnonce'=>$annonce]);
        $form = $this->createForm(TerrainConstructibleType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() ) {
            if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
                if ($_FILES['picProfilCons']['name'] !== '') {
                    $path = $_FILES['picProfilCons']['name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picProfilCons']['tmp_name']);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $annonce->setImgProfil($base64);
                }

            $em->persist($annonce);
            $em->flush();
            for ($i=0; $i<sizeof($_FILES['picConstructible']['name']); $i++){
                $path = $_FILES['picConstructible']['name'][$i];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picConstructible']['tmp_name'][$i]);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $image = new Images();
                $image->setImg($base64)
                    ->setIdAnnonce($annonce);


                $em->persist($image);
                $em->flush();
                $this->addFlash(
                    'info',
                    'Annonce Bien Modifiée'
                );
            }
            return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));
        }else {
                return $this->render('terrain_constructible/edit.html.twig', array(
                    'annonce' => $annonce,
                    'form' => $form->createView(),
                    'images'=>$images
                ));
            }
        }
        return $this->render('terrain_constructible/edit.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
            'images'=>$images
        ));
    }

    /**
     * @Route("constructible/{id}", name="show_terrain_constructible", methods={"GET"},requirements={"id":"\d+"})
     */
    public function show(Annonce $annonce , ImagesRepository $imagesRepository ): Response
    {

        $images = $imagesRepository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('terrain_constructible/show.html.twig', [
            'annonce' => $annonce,
            'id' => 5,
            'images'=>$images,

        ]);
    }

    /**
     * @Route("/afficher/constructible/{id}", name="constructible_afficher_one" )
     */
    public function afficherOne(Annonce $annonce, ImagesRepository $repository , AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce'=>$annonce]);
        $avis = $avisRepository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('terrain_constructible/afficherVente.html.twig', [
            'constructible' =>  $annonce,
            'images' =>  $images,
            'avis' => $avis,

        ]);
    }
    /**
     * @Route("/message/envoyer/constructible/{action}/{id}", name="send_msg_constructible")
     */
    public function sendMessage(Request $request, $action, MessageRepository $messageRepository , Annonce $annonce, ImagesRepository $repository, AvisRepository $avisRepository)
    {

        $images = $repository->findBy(['idAnnonce' => $annonce]);
        $avis = $avisRepository->findBy(['idAnnonce' => $annonce]);
        $msg = new Message();
        $msg->setMsg($request->request->get('message'))
            ->setDataTime(new \DateTime())
            ->setEnvoyerPar("User")
            ->setUser($this->getUser())
            ->setRecuPar("Admin")
            ->setIdAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($msg);
        $this->getDoctrine()->getManager()->flush();
        //dump($messageRepository->findAll());exit;

        if ($action == "acheter") {
            return $this->render('terrain_constructible/afficherVente.html.twig', [
                'images' => $images,
                'constructible' => $annonce,
                'avis' => $avis,
                'message' => 'message envoyé',
            ]);
        }
    }

    /**
     * @Route("/commenter/constructible/{id}", name="commenter_constructible")
     */
    public function CommenterMaison(Annonce $annonce, Request $request)
    {
        //dump($request->request->all());exit;

        $avis = new Avis();
        $avis->setCommentaire($request->request->get('commentaire'))
            ->setdate(new\DateTime())
            ->setIduser($this->getUser())
            ->setidAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($avis);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('constructible_afficher_one',['id'=>$annonce->getId()]);
    }
}
