<?php

namespace App\Controller;

use App\Entity\Images;
use App\Entity\Map;
use App\Entity\Annonce;
use App\Form\AnnonceType;
use App\Form\AppartementType;
use App\Form\LocalCommercialType;
use App\Form\MaisonType;
use App\Form\StudioType;
use App\Form\TerrainAgricoleType;
use App\Form\TerrainConstructibleType;
use App\Form\TerrainIndustrielType;
use App\Form\VillaType;
use App\Repository\AnnonceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AnnonceController extends AbstractController
{
    /**
     * @Route("/aa", name="annonce_index", methods={"GET"})
     */
    public function index(AnnonceRepository $annonceRepository): Response
    {
        return $this->render('annonce/index.html.twig', [
            'annonces' => $annonceRepository->findBy(['idUserId' => $this->getUser() , 'active'=> 1 ]),
        ]);
    }

    /**
     * @Route("/new/{id}", name="annonce_new", methods={"GET","POST"})
     */
    public function new(Request $request, $id): Response
    {

        $annonce = new Annonce();

        $formAppartement = $this->createForm(AppartementType::class, $annonce);
        $formMaison = $this->createForm(MaisonType::class, $annonce);
        $formVilla = $this->createForm(VillaType::class, $annonce);
        $formTerrainIndustriel = $this->createForm(TerrainIndustrielType::class, $annonce);
        $formTerrainAgricole = $this->createForm(TerrainAgricoleType::class, $annonce);
        $formTerrainConstructible = $this->createForm(TerrainConstructibleType::class, $annonce);
        $formStudio = $this->createForm(StudioType::class, $annonce);
        $formLocalCommercial = $this->createForm(LocalCommercialType::class, $annonce);


        $formAppartement->handleRequest($request);
        $formMaison->handleRequest($request);
        $formVilla->handleRequest($request);
        $formTerrainIndustriel->handleRequest($request);
        $formTerrainAgricole->handleRequest($request);
        $formTerrainConstructible->handleRequest($request);
        $formLocalCommercial->handleRequest($request);
        $formStudio->handleRequest($request);

        if ($formAppartement->isSubmitted()) {
            $id = 0;
            if ($formAppartement->isValid()) {
                $map = $request->get('maap');
                $latitude = substr($map, 1, strpos($map, ',') - 1);
                $longitude = substr($map, strpos($map, ',') + 2, (strpos($map, ')') - strpos($map, ',')) - 2);

                $path = $_FILES['picProfil']['name'];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picProfil']['tmp_name']);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                $entityManager = $this->getDoctrine()->getManager();
                $annonce->setIdUserId($this->getUser()->getId())
                    ->setNature('Appartement')
                    ->setDateAdd(new \DateTime())
                    ->setReference(self::getReference())
                    ->setActive(1)
                    ->setImgProfil($base64);


                $entityManager->persist($annonce);
                $entityManager->flush();

                $maps = new Map();
                $maps->setLatitude(floatval($latitude))
                        ->setLongitude(floatval($longitude))
                        ->setIdAnnonce($annonce);
                //dump($maps);exit;
                $entityManager->persist($maps);
                $entityManager->flush();

                for ($i = 0; $i < sizeof($_FILES['pic']['name']); $i++) {
                    $path = $_FILES['pic']['name'][$i];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['pic']['tmp_name'][$i]);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                    $image = new Images();
                    $image->setImg($base64)
                        ->setIdAnnonce($annonce);

                    $entityManager->persist($image);
                    $entityManager->flush();
                    $this->addFlash(
                        'info',
                        'Annonce Bien Ajoutée'
                    );
                }
                return $this->redirectToRoute('annonce_index');
            }
        }
        if ($formMaison->isSubmitted()) {
            $id = 1;
            if ($formMaison->isValid()) {
                $map = $request->get('maap');
                $latitude = substr($map, 1, strpos($map, ',') - 1);
                $longitude = substr($map, strpos($map, ',') + 2, (strpos($map, ')') - strpos($map, ',')) - 2);
                $path = $_FILES['picProfilMaison']['name'];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picProfilMaison']['tmp_name']);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                $entityManager = $this->getDoctrine()->getManager();

                $annonce->setIdUserId($this->getUser()->getId())
                    ->setNature('Maison')
                    ->setDateAdd(new \DateTime())
                    ->setReference(self::getReference())
                    ->setActive(1)
                    ->setImgProfil($base64);

                $entityManager->persist($annonce);
                $entityManager->flush();

                $maps = new Map();
                $maps->setLatitude(floatval($latitude))
                    ->setLongitude(floatval($longitude))
                    ->setIdAnnonce($annonce);
                //dump($maps);exit;
                $entityManager->persist($maps);
                $entityManager->flush();

                for ($i = 0; $i < sizeof($_FILES['picMaison']['name']); $i++) {

                    $path = $_FILES['picMaison']['name'][$i];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picMaison']['tmp_name'][$i]);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $image = new Images();
                    $image->setImg($base64)
                        ->setIdAnnonce($annonce);

                    $entityManager->persist($image);
                    $entityManager->flush();
                    $this->addFlash(
                        'info',
                        'Annonce Bien Ajoutée'
                    );
                }
                return $this->redirectToRoute('annonce_index');
            }
        }
        if ($formVilla->isSubmitted()) {
            $id = 2;
            if ($formVilla->isValid()) {

                $path = $_FILES['picProfilVilla']['name'];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picProfilVilla']['tmp_name']);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                $entityManager = $this->getDoctrine()->getManager();
                $annonce->setIdUserId($this->getUser()->getId())
                    ->setNature('Villa')
                    ->setDateAdd(new \DateTime())
                    ->setReference(self::getReference())
                    ->setImgProfil($base64);
                $entityManager->persist($annonce);
                $entityManager->flush();

                for ($i = 0; $i < sizeof($_FILES['picVilla']['name']); $i++) {
                    $path = $_FILES['picVilla']['name'][$i];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picVilla']['tmp_name'][$i]);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $image = new Images();
                    $image->setImg($base64)
                        ->setIdAnnonce($annonce);


                    $entityManager->persist($image);
                    $entityManager->flush();
                    $this->addFlash(
                        'info',
                        'Annonce Bien Ajoutée'
                    );
                }
                return $this->redirectToRoute('annonce_index');
            }
        }
        if ($formTerrainAgricole->isSubmitted()) {
            $id = 3;
            if ($formTerrainAgricole->isValid()) {
                $path = $_FILES['picProfilAgricole']['name'];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picProfilAgricole']['tmp_name']);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                $entityManager = $this->getDoctrine()->getManager();
                $annonce->setIdUserId($this->getUser()->getId())
                    ->setNature('Terrain Agricole')
                    ->setDateAdd(new \DateTime())
                    ->setReference(self::getReference())
                    ->setActive(1)
                    ->setImgProfil($base64);

                $entityManager->persist($annonce);
                $entityManager->flush();

                for ($i = 0; $i < sizeof($_FILES['picAgrico']['name']); $i++) {
                    $path = $_FILES['picAgrico']['name'][$i];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picAgrico']['tmp_name'][$i]);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $image = new Images();
                    $image->setImg($base64)
                        ->setIdAnnonce($annonce);


                    $entityManager->persist($image);
                    $entityManager->flush();
                    $this->addFlash(
                        'info',
                        'Annonce Bien Ajoutée'
                    );
                }
                return $this->redirectToRoute('annonce_index');
            }
        }
        if ($formTerrainIndustriel->isSubmitted()) {
            $id = 4;
            if ($formTerrainIndustriel->isValid()) {
                $path = $_FILES['picProfilIndustriel']['name'];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picProfilIndustriel']['tmp_name']);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                $entityManager = $this->getDoctrine()->getManager();
                $annonce->setIdUserId($this->getUser()->getId())
                    ->setNature('Terrain Industriel')
                    ->setDateAdd(new \DateTime())
                    ->setReference(self::getReference())
                    ->setImgProfil($base64);

                $entityManager->persist($annonce);
                $entityManager->flush();
                for ($i = 0; $i < sizeof($_FILES['picIndus']['name']); $i++) {
                    $path = $_FILES['picIndus']['name'][$i];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picIndus']['tmp_name'][$i]);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $image = new Images();
                    $image->setImg($base64)
                        ->setIdAnnonce($annonce);


                    $entityManager->persist($image);
                    $entityManager->flush();
                    $this->addFlash(
                        'info',
                        'Annonce Bien Ajoutée'
                    );
                }
                return $this->redirectToRoute('annonce_index');
            }
        }
        if ($formTerrainConstructible->isSubmitted()) {
            $id = 5;
            if ($formTerrainConstructible->isValid()) {
                $path = $_FILES['picProfilConstructible']['name'];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picProfilConstructible']['tmp_name']);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                $entityManager = $this->getDoctrine()->getManager();
                $annonce->setIdUserId($this->getUser()->getId())
                    ->setNature('Terrain Constructible')
                    ->setDateAdd(new \DateTime())
                    ->setReference(self::getReference())
                    ->setActive(1)
                    ->setImgProfil($base64);

                $entityManager->persist($annonce);
                $entityManager->flush();

                for ($i = 0; $i < sizeof($_FILES['picConstructible']['name']); $i++) {
                    $path = $_FILES['picConstructible']['name'][$i];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picConstructible']['tmp_name'][$i]);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $image = new Images();
                    $image->setImg($base64)
                        ->setIdAnnonce($annonce);


                    $entityManager->persist($image);
                    $entityManager->flush();
                    $this->addFlash(
                        'info',
                        'Annonce Bien Ajoutée'
                    );
                }
                return $this->redirectToRoute('annonce_index');
            }
        }
        if ($formLocalCommercial->isSubmitted()) {
            $id = 6;
            if ($formLocalCommercial->isValid()) {
                $path = $_FILES['picProfilLocal']['name'];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picProfilLocal']['tmp_name']);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $entityManager = $this->getDoctrine()->getManager();
                $annonce->setIdUserId($this->getUser()->getId())
                    ->setNature('Local Commercial')
                    ->setDateAdd(new \DateTime())
                    ->setReference(self::getReference())
                    ->setActive(1)
                    ->setImgProfil($base64);

                $entityManager->persist($annonce);
                $entityManager->flush();

                for ($i = 0; $i < sizeof($_FILES['picCommercial']['name']); $i++) {
                    $path = $_FILES['picCommercial']['name'][$i];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picCommercial']['tmp_name'][$i]);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $image = new Images();
                    $image->setImg($base64)
                        ->setIdAnnonce($annonce);

                    $entityManager->persist($image);
                    $entityManager->flush();
                    $this->addFlash(
                        'info',
                        'Annonce Bien Ajoutée'
                    );
                }
                return $this->redirectToRoute('annonce_index');
            }

        }
        if ($formStudio->isSubmitted()) {
            $id = 7;
            if ($formStudio->isValid()) {
                $path = $_FILES['picProfilStudio']['name'];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picProfilStudio']['tmp_name']);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                $entityManager = $this->getDoctrine()->getManager();
                $annonce->setIdUserId($this->getUser()->getId())
                    ->setNature('Studio')
                    ->setDateAdd(new \DateTime())
                    ->setReference(self::getReference())
                    ->setActive(1)
                    ->setImgProfil($base64);

                $entityManager->persist($annonce);
                $entityManager->flush();

                for ($i = 0; $i < sizeof($_FILES['picStudio']['name']); $i++) {
                    $path = $_FILES['picStudio']['name'][$i];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picStudio']['tmp_name'][$i]);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $image = new Images();
                    $image->setImg($base64)
                        ->setIdAnnonce($annonce);
                    $entityManager->persist($image);
                    $entityManager->flush();
                    $this->addFlash(
                        'info',
                        'Annonce Bien Ajoutée'
                    );
                }
                return $this->redirectToRoute('annonce_index');
            }

        }


        return $this->render('annonce/new.html.twig', [
//            'annonce' => $annonce,
            'formAppartement' => $formAppartement->createView(),
            'formMaison' => $formMaison->createView(),
            'formVilla' => $formVilla->createView(),
            'formTerrainIndustriel' => $formTerrainIndustriel->createView(),
            'formTerrainAgricole' => $formTerrainAgricole->createView(),
            'formTerrainConstructible' => $formTerrainConstructible->createView(),
            'formStudio' => $formStudio->createView(),
            'formLocalCommercial' => $formLocalCommercial->createView(),
            'id' => $id
        ]);
    }


    /**
     * @Route("/supprimer/{id}", name="annonce_delete")
     */
    public function delete(Request $request, Annonce $annonce): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $annonce->setActive(0);
        $entityManager->persist($annonce);
        $entityManager->flush();

        return $this->redirectToRoute('annonce_index');
    }

    public static function getReference($dRefLength = 8)
    {
        $sref = "REF-";
        $sCharacters = "0123456789";

        for ($i = 1; $i <= $dRefLength; $i++) {
            $randPosition = mt_rand(0, (strlen($sCharacters) - 1));
            $sref .= $sCharacters[$randPosition];
        }

        return $sref;
    }
}
