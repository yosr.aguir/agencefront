<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\Images;
use App\Entity\Avis;
use App\Repository\AnnonceRepository;
use App\Repository\AvisRepository;
use App\Repository\MessageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\AppartementType;
use App\Repository\ImagesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Annonce;

class AppartementController extends AbstractController
{
    /**
     * @Route("/appartement", name="appartement")
     */
    public function index()
    {
        return $this->render('appartement/index.html.twig', [
            'controller_name' => 'AppartementController',
        ]);
    }

    /**
     * @Route("/vente/appartement", name="appartement_vente")
     *    Method({"GET" , "POST"})
     */
    public function vente(AnnonceRepository $repository)
    {
        $appartementVente = $repository->findBy(['nature' => 'Appartement', 'typeAction' => 'Acheter']);
        return $this->render('accueil/vente_appartement.html.twig', [
            'appartement' => $appartementVente,
        ]);
    }

    /**
     * @Route("/location/appartement", name="appartement_location")
     *    Method({"GET" , "POST"})
     */
    public function location(AnnonceRepository $repository)
    {
        $appartementLocation = $repository->findBy(['nature' => 'Appartement', 'typeAction' => 'Louer']);
        return $this->render('accueil/location_appartement.html.twig', [
            'appartement' => $appartementLocation,
        ]);
    }

    /**
     * @Route("/appartement/edit/{id}" , name="appartement_edit",requirements={"id":"\d+"})
     * Method({"GET" , "POST"})
     */


    public function edit(Request $request, Annonce $annonce, ImagesRepository $imagesRepository)
    {

        $images = $imagesRepository->findBy(['idAnnonce' => $annonce]);
        $form = $this->createForm(AppartementType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() ) {
            if ($form->isValid()) {
                //dump($_FILES);exit;
                $em = $this->getDoctrine()->getManager();
                if ($_FILES['picProfil']['name'] !== '') {
                    $path = $_FILES['picProfil']['name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picProfil']['tmp_name']);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $annonce->setImgProfil($base64);

                }
                $em->persist($annonce);
                $em->flush();

                for ($i = 0; $i < sizeof($_FILES['pic']['name']); $i++) {
                    $path = $_FILES['pic']['name'][$i];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['pic']['tmp_name'][$i]);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                    $image = new Images();
                    $image->setImg($base64)
                        ->setIdAnnonce($annonce);

                    $em->persist($image);
                    $em->flush();
                    $this->addFlash(
                        'info',
                        'Annonce Bien Modifiée'
                    );
                }
                return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));
            } else {
                return $this->render('appartement/edit.html.twig', array(
                    'annonce' => $annonce,
                    'form' => $form->createView(),
                    'images' => $images

                ));
            }
        }

        return $this->render('appartement/edit.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
            'images' => $images

        ));
    }

    /**
     * @Route("/appartement/{id}", name="appartement_show", methods={"GET"},requirements={"id":"\d+"})
     */
    public function show(Annonce $annonce, ImagesRepository $imagesRepository): Response
    {

        $images = $imagesRepository->findBy(['idAnnonce' => $annonce]);
        return $this->render('appartement/show.html.twig', [
            'annonce' => $annonce,
            'id' => 0,
            'images' => $images,

        ]);
    }

    /**
     * @Route("/location/appartement/{id}", name="appartement_afficher_location" )
     */
    public function afficherLocation(Annonce $annonce, ImagesRepository $repository ,AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce' => $annonce]);
        $avis =$avisRepository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('appartement/afficherLocation.html.twig', [
            'appartements' => $annonce,
            'images' => $images,
            'avis'=> $avis,

        ]);
    }

    /**
     * @Route("/afficher/appartement/{id}", name="appartement_afficher_one" )
     */
    public function afficherOne(Annonce $annonce, ImagesRepository $repository ,AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce' => $annonce]);
        $avis =$avisRepository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('appartement/afficherVente.html.twig', [
            'appartements' => $annonce,
            'images' => $images,
            'avis'=> $avis,

        ]);
    }

    /**
     * @Route("/error/message/appartement/" , name="error_message")
     */
    public function verif(Request $request): Response
    {
        {
            if (!$this->getUser()) {
                return $this->redirectToRoute('security_login');

            }
        }
    }


    /**
     * @Route("/message/envoyer/appartement/{action}/{id}", name="send_msg_appartement")
     */
    public function sendMessage($action,Annonce $annonce, Request $request, MessageRepository $messageRepository, ImagesRepository $repository , AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce' => $annonce]);
        $avis = $avisRepository->findBy(['idAnnonce' => $annonce]);
        $msg = new Message();
        $msg->setMsg($request->request->get('message'))
            ->setDataTime(new \DateTime())
            ->setEnvoyerPar("User")
            ->setUser($this->getUser())
            ->setRecuPar("Admin")
            ->setIdAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($msg);
        $this->getDoctrine()->getManager()->flush();
        //dump($messageRepository->findAll());exit;

        if ($action == "acheter") {

            return $this->render('appartement/afficherVente.html.twig', [
                'images' => $images,
                'appartements' => $annonce,
                'avis'=> $avis,
                'message'=>'message envoyé',
            ]);
        } else {
            return $this->render('appartement/afficherLocation.html.twig', [
                'images' => $images,
                'appartements' => $annonce,
                'avis'=> $avis,
                'message'=>'message envoyé',
            ]);
        }
    }

    /**
     * @Route("/commenter/appartement/{id}", name="commenter_appartement")
     */
    public function CommenterMaison(Annonce $annonce, Request $request)
    {
        //dump($request->request->all());exit;

        $avis = new Avis();
        $avis->setCommentaire($request->request->get('commentaire'))
            ->setdate(new\DateTime())
            ->setIduser($this->getUser())
            ->setidAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($avis);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('appartement_afficher_one',['id'=>$annonce->getId()]);
    }

}
