<?php

namespace App\Controller;

use App\Repository\AvisRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\LocalCommercialType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Annonce;
use App\Entity\Message;
use App\Entity\Images;
use App\Entity\Avis;
use App\Repository\ImagesRepository;
use App\Repository\AnnonceRepository;
use App\Repository\MessageRepository;
class LocalCommercialController extends AbstractController
    /**
     * @Route("/local")
     */

{

    /**
     * @Route("/local/commercial", name="local_commercial")
     */
    public function index()
    {
        return $this->render('local_commercial/index.html.twig', [
            'controller_name' => 'LocalCommercialController',
        ]);
    }

    /**
     * @Route("/vente/localCommercial", name="local_commercial_vente")
     */
    public function vente(AnnonceRepository $repository)
    {
        $localCommercialVente = $repository->findBy(['nature' => 'Local Commercial', 'typeAction' => 'Acheter']);
        return $this->render('accueil/vente_local_commercial.html.twig', [
            'localCommercial' => $localCommercialVente,
        ]);
    }

    /**
     * @Route("/location/localCommercial", name="local_commercial_location")
     *    Method({"GET" , "POST"})
     */
    public function location(AnnonceRepository $repository)
    {
        $localCommercialLocation = $repository->findBy(['nature' => 'Local Commercial', 'typeAction' => 'Louer']);
        return $this->render('accueil/location_local_commercial.html.twig', [
            'localCommercial' => $localCommercialLocation,
        ]);
    }

    /**
     * @Route("/LocalCommercial/edit/{id}" , name="local_commercial_edit")
     * Method({"GET" , "POST"})
     */
    public function edit(Request $request, Annonce $annonce, ImagesRepository $imagesRepository)
    {

        $images = $imagesRepository->findBy(['idAnnonce' => $annonce]);
        $form = $this->createForm(LocalCommercialType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted()){
           if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
               if ($_FILES['picProfilLocal']['name'] !== '') {
                   $path = $_FILES['picProfilLocal']['name'];
                   $type = pathinfo($path, PATHINFO_EXTENSION);
                   $data = file_get_contents($_FILES['picProfilLocal']['tmp_name']);
                   $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                   $annonce->setImgProfil($base64);
               }
            $em->persist($annonce);
            $em->flush();

            for ($i=0; $i<sizeof($_FILES['picCommercial']['name']); $i++) {
                $path = $_FILES['picCommercial']['name'][$i];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picCommercial']['tmp_name'][$i]);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $image = new Images();
                $image->setImg($base64)
                    ->setIdAnnonce($annonce);

                $em->persist($image);
                $em->flush();
                $this->addFlash(
                    'info',
                    'Annonce Bien Modifiée'
                );
            }

            return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));
        } else {
               return $this->render('local_commercial/edit.html.twig', array(
                   'annonce' => $annonce,
                   'form' => $form->createView(),
                   'images' => $images
               ));
           }
        }




        return $this->render('local_commercial/edit.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
            'images' => $images
        ));
    }

    /**
     * @Route("/{id}", name="local_commercial_show", methods={"GET"})
     */
    public function show(Annonce $annonce, ImagesRepository $imagesRepository): Response
    {

        $images = $imagesRepository->findBy(['idAnnonce' => $annonce]);
        return $this->render('local_commercial/show.html.twig', [
            'annonce' => $annonce,
            'id' => 6,
            'images' => $images,
        ]);
    }

    /**
     * @Route("/afficher/local/{id}", name="local_afficher_one" )
     */
    public function afficherOne(Annonce $annonce, ImagesRepository $repository ,AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce' => $annonce]);
        $avis = $avisRepository->findBy(['idAnnonce' => $annonce]);
        return $this->render('local_commercial/afficherVente.html.twig', [
            'local' => $annonce,
            'images' => $images,
            'avis' => $avis,

        ]);
    }

    /**
     * @Route("/location/local/{id}", name="local_afficher_location" )
     */
    public function afficherLocation(Annonce $annonce, ImagesRepository $repository , AvisRepository  $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce' => $annonce]);
        $avis = $avisRepository->findBy(['idAnnonce' => $annonce]);
        return $this->render('local_commercial/afficherLocation.html.twig', [
            'local' => $annonce,
            'images' => $images,
            'avis' => $avis,
        ]);
    }


    /**
     * @Route("/message/envoyer/local/{action}/{id}", name="send_msg_local")
     */
    public function sendMessage(Annonce $annonce,Request $request, $action, MessageRepository $messageRepository , ImagesRepository $repository ,AvisRepository $avisRepository)
    {

        $images = $repository->findBy(['idAnnonce' => $annonce]);
        $avis = $avisRepository->findBy(['idAnnonce' => $annonce]);
        $msg = new Message();
        $msg->setMsg($request->request->get('message'))
            ->setDataTime(new \DateTime())
            ->setEnvoyerPar("User")
            ->setUser($this->getUser())
            ->setRecuPar("Admin")
            ->setIdAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($msg);
        $this->getDoctrine()->getManager()->flush();
        //dump($messageRepository->findAll());exit;

        if ($action == "acheter") {
            return $this->render('local_commercial/afficherVente.html.twig', [
                'images' => $images,
                'local' => $annonce,
                'avis'=> $avis,
                'message' => 'message envoyé',
            ]);
        } else {
            return $this->render('local_commercial/afficherLocation.html.twig', [
                'images' => $images,
                'local' => $annonce,
                'avis'=> $avis,
                'message' => 'message envoyé',
            ]);
        }
    }

    /**
     * @Route("/commenter/local/{id}", name="commenter_local")
     */
    public function Commenterlocal(Annonce $annonce, Request $request)
    {
        //dump($request->request->all());exit;

        $avis = new Avis();
        $avis->setCommentaire($request->request->get('commentaire'))
            ->setdate(new\DateTime())
            ->setIduser($this->getUser())
            ->setidAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($avis);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('local_afficher_one',['id'=>$annonce->getId()]);
    }
    }