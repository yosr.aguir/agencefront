<?php

namespace App\Controller;

use App\Repository\AnnonceRepository;
use App\Repository\AvisRepository;
use App\Repository\ImagesRepository;
use App\Repository\MapRepository;
use App\Repository\MessageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\Avis;
use App\Entity\Message;

class RechercherController extends AbstractController
{
    /**
     * @Route("/recherche", name="search")
     */
    public function index(Request $request, AnnonceRepository $repository , MapRepository $mapRepository)
    {
        $result = [];

        switch ($request->get('sweets')) {

            case 'Appartement':
                $action = $request->get('AppartementAction');
                $ville = $request->get('AppartementVille');
                $nbrEtage = $request->get('AppartementEtage');
                $prix = $request->get('AppartementPrix');
                $surface = $request->get('AppartementSurface');
                $result = $repository->getAppartement($action, $ville, $nbrEtage, $surface, $prix);
                //dump($request->get('sweets'));exit;
                break;
            case 'Maison':
                $action = $request->get('MaisonAction');
                $ville = $request->get('MaisonVille');
                $prix = $request->get('MaisonPrix');
                $surface = $request->get('MaisonSurface');
                $nbrPieces = $request->get('MaisonPieces');
                $result = $repository->getMaison($action, $ville, $nbrPieces, $surface, $prix);
                break;
            case 'Villa ':
                $action = $request->get('VillaAction');
                $ville = $request->get('VillaVille');
                $prix = $request->get('VillaPrix');
                $surface = $request->get('VillaSurface');
                $nbrPieces = $request->get('VillaPieces');
                $result = $repository->getVilla($action, $ville, $nbrPieces, $surface, $prix);

                break;

            case 'Terrain Agricole':
                $action = $request->get('AgricoleAction');
                $ville = $request->get('AgricoleVille');
                $prix = $request->get('AgricolePrix');
                $surface = $request->get('AgricoleSurface');
                $result = $repository->getAgricole($action, $ville, $surface, $prix);
                break;
            case 'Terrain Constructible':
                $action = $request->get('ConstructibleAction');
                $ville = $request->get('ConstructibleVille');
                $prix = $request->get('ConstructiblePrix');
                $surface = $request->get('ConstructibleSurface');
                $result = $repository->getConstructible($action, $ville, $surface, $prix);
                break;
            case 'Terrain Industriel':
                $action = $request->get('IndustrielAction');
                $ville = $request->get('IndustrielVille');
                $prix = $request->get('IndustrielPrix');
                $surface = $request->get('IndustrielSurface');
                $result = $repository->getIndustriel($action, $ville, $surface, $prix);
                break;
            case 'Local Commercial':
                $action = $request->get('LocalAction');
                $ville = $request->get('LocalVille');
                $prix = $request->get('LocalPrix');
                $surface = $request->get('LocalSurface');
                $nbrPieces = $request->get('LocalPieces');
                $result = $repository->getLocal($action, $ville, $surface, $prix, $nbrPieces);
                break;
            case 'Studio':
                $action = $request->get('StudioAction');
                $ville = $request->get('studioVille');
                $prix = $request->get('studioPrix');
                $surface = $request->get('studioSurface');
                $result = $repository->getStudio($action, $ville, $surface, $prix);
                break;
            //dump($result);exit;
        }
        if ($result) {
            $map=[];
            foreach ($result as $r){
                $m = $mapRepository->findOneByAnnonce($r['id']);
                array_push($map,['id'=>$m[0]['id'],'latitude'=>$m[0]['latitude'],'longitude'=>$m[0]['longitude'],'prix'=>$m[0]['prix'],'idAnnonce'=>$m[0]['idAnnonce']]);
            }

            return $this->render('rechercher/index.html.twig', [
                'result' => $result,
                'map'=> $map

            ]);
        } else {
            return $this->render('rechercher/index.html.twig', [
                'result' => [],
            ]);
        }
    }


    /**
     * @Route("/annonce/one/{id}", name="afficher_resultat_app" )
     */
    public function afficherOne(Annonce $annonce, ImagesRepository $repository, AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce' => $annonce]);
        $avis = $avisRepository->findBy(['idAnnonce' => $annonce]);
        return $this->render('rechercher\afficheOne.html.twig', [
            'annonce' => $annonce,
            'images' => $images,
            'avis' => $avis,
            //'appartements' =>$annonce,
        ]);

    }

    /**
     * @Route("/commenter/resultat/{id}", name="commenter_resultat")
     */
    public function CommenterMaison(Annonce $annonce, Request $request)
    {
        //dump($request->request->all());exit;

        $avis = new Avis();
        $avis->setCommentaire($request->request->get('commentaire'))
            ->setdate(new\DateTime())
            ->setIduser($this->getUser())
            ->setidAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($avis);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('afficher_resultat_app', ['id' => $annonce->getId()]);
    }

    /**
     * @Route("/message/envoyer/resultat/{id}", name="send_msg_resultat")
     */
    public function sendMessage(Annonce $annonce, Request $request, MessageRepository $messageRepository, ImagesRepository $repository, AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce' => $annonce]);
        $avis = $avisRepository->findBy(['idAnnonce' => $annonce]);
        $msg = new Message();
        $msg->setMsg($request->request->get('message'))
            ->setDataTime(new \DateTime())
            ->setEnvoyerPar("User")
            ->setUser($this->getUser())
            ->setRecuPar("Admin")
            ->setIdAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($msg);
        $this->getDoctrine()->getManager()->flush();
        //dump($messageRepository->findAll());exit;


        return $this->render('rechercher/afficheOne.html.twig', [
            'images' => $images,
            'annonce' => $annonce,
            'avis' => $avis,
            'message' => 'message envoyé',
        ]);

    }
}