<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Form\UtilisateurEditPasswordType;
use App\Form\UtilisateurEditType;
use App\Form\UtilisateurType;
use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class ProfilController extends AbstractController
{
    /**
     * @Route("/profil", name="profil")
     */
    public function index()
    {
        return $this->render('profil/index.html.twig', [
            'controller_name' => 'ProfilController',
        ]);
    }

    /**
     * @Route("/profil/edit/{id}" , name="profil_edit")
     */
    public function editProfil(Request $request, Utilisateur $utilisateur, UtilisateurRepository $repository, UserPasswordEncoderInterface $encoder)
    {

        $form = $this->createForm(UtilisateurEditType::class, $utilisateur);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if ($_FILES['pic']['name'] !== '') {
                    $path = $_FILES['pic']['name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['pic']['tmp_name']);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $utilisateur->setImg($base64);
                }
                $em->persist($utilisateur);
                $em->flush();
                return $this->redirectToRoute('dashboard_user');
            } else {
                return $this->render('dashboard/profil_edit.html.twig', array(
                    'utilisateur' => $utilisateur,
                    'form' => $form->createView(),

                ));
            }
        }

        return $this->render('dashboard/profil_edit.html.twig', array(
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),

        ));
    }

    /**
     * @Route("/profil/editpassword/{id}" , name="profil_edit_password")
     */
    public function editPassword(Request $request, Utilisateur $utilisateur, UtilisateurRepository $repository, UserPasswordEncoderInterface $encoder)
    {

        $AncienPassword = $utilisateur->getPassword();
        $form = $this->createForm(UtilisateurEditPasswordType::class, $utilisateur);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $hash = $encoder->encodePassword($utilisateur, $form->get('AncienPassword')->getData());
                if ($AncienPassword == $hash) {
                    //dump("kifkif");exit;
                    $hashNv = $encoder->encodePassword($utilisateur, $form->get('password')->getData());
                }

                $em = $this->getDoctrine()->getManager();
                $utilisateur->setPassword($hashNv);


                $em->persist($utilisateur);
                $em->flush();
                return $this->redirectToRoute('dashboard_user');
            } else {
                return $this->render('dashboard/editPassword.html.twig', array(
                    'utilisateur' => $utilisateur,
                    'form' => $form->createView(),
                    'message' => 'Votre ancien passowrd non valide'
                ));
            }
        }
        return $this->render('dashboard/editPassword.html.twig', array(
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),

        ));
    }
}

