<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Form\UtilisateurType;
use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/connexion", name="security_login")
     */
    public function login(AuthenticationUtils $utils, RequestStack $requestStack)
    {
        $session = $requestStack->getCurrentRequest()->getSession();
        if ($session->get('_security_main')){
            return $this->redirectToRoute('dashboard');
        } /*else {
            dump($session);exit;
        }*/

        return $this->render('security/login.html.twig',[
            'error'=>$utils->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Route("/deconnexion", name="security_logout")
     */
    public function logout()
    {
    }
    /**
     * @Route("/inscription", name="security_register", methods={"GET","POST"})
     */
    public function register(UserPasswordEncoderInterface $encoder, Request $request, UtilisateurRepository $repository)
    {
        $utilisateur = new Utilisateur();
        $form = $this->createForm(UtilisateurType::class, $utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $us = $repository->findOneBy(['email'=>$utilisateur->getEmail()]);
            if (!empty($us)){
                return $this->render('security/register.html.twig',[
                    'form'=>$form->createView(),
                    'mailErreur'=>'Email existe déja'
                ]);
            } else {
                // Convert picture to base64
                $path = $_FILES['utilisateur']['name']['img'];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['utilisateur']['tmp_name']['img']);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                $hash = $encoder->encodePassword($utilisateur,$utilisateur->getPassword());
                $utilisateur->setActive(true)
                    ->setPassword($hash)
                    ->setImg($base64);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($utilisateur);
                $entityManager->flush();
                //$this->addFlash('success', 'Votre compte à bien été enregistré.');

                return $this->redirectToRoute('security_login');
            }

        }

        return $this->render('security/register.html.twig',[
            'form'=>$form->createView()
        ]);
    }
}
