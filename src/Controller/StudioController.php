<?php

namespace App\Controller;
use App\Form\StudioType;
use App\Repository\AvisRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\Avis;
use App\Entity\Images;
use App\Repository\ImagesRepository;
use App\Repository\AnnonceRepository;
use App\Repository\MessageRepository;

class StudioController extends AbstractController
{
    /**
     * @Route("/studio", name="studio")
     */
    public function index()
    {
        return $this->render('studio/index.html.twig', [
            'controller_name' => 'StudioController',
        ]);
    }
    /**
     * @Route("/vente/studio", name="studio_vente")
     *    Method({"GET" , "POST"})
     */
    public function vente(AnnonceRepository $repository)
    {
        $studioVente = $repository->findBy(['nature'=>'Studio' ,'typeAction'=>'Acheter']);
        return $this->render('accueil/vente_studio.html.twig', [
            'studio' => $studioVente,
        ]);
    }
    /**
     * @Route("/location/studio", name="studio_location")
     *    Method({"GET" , "POST"})
     */
    public function location(AnnonceRepository  $repository)
    {
        $studioLocation = $repository->findBy(['nature'=>'studio' , 'typeAction'=>'Louer']);
        return $this->render('accueil/location_studio.html.twig', [
            'studio' => $studioLocation,
        ]);
    }
    /**
     * @Route("/studio/edit/{id}" , name="studio_edit",requirements={"id":"\d+"})
     * Method({"GET" , "POST"})
     */
    public function edit(Request $request, Annonce $annonce ,ImagesRepository $imagesRepository)
    {


        $images = $imagesRepository->findBy(['idAnnonce'=>$annonce]);
        $form = $this->createForm(StudioType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted()){
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if ($_FILES['picProfilStudio']['name'] !== '') {
                    $path = $_FILES['picProfilStudio']['name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picProfilStudio']['tmp_name']);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $annonce->setImgProfil($base64);

                }
            $em->persist($annonce);
            $em->flush();

            for ($i=0; $i<sizeof($_FILES['picStudio']['name']); $i++) {
                $path = $_FILES['picStudio']['name'][$i];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picStudio']['tmp_name'][$i]);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $image = new Images();
                $image->setImg($base64)
                    ->setIdAnnonce($annonce);
                $em->persist($image);
                $em->flush();
                $this->addFlash(
                    'info',
                    'Annonce Bien Modifiée'
                );
            }
            return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));
        }else {
                return $this->render('studio/edit.html.twig', array(
                    'annonce' => $annonce,
                    'form' => $form->createView(),
                    'images'=>$images,
                ));
            }
        }
        return $this->render('studio/edit.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
            'images'=>$images,
        ));
    }

    /**
     * @Route("/studio/{id}", name="studio_show", methods={"GET"},requirements={"id":"\d+"})
     */
    public function show(Annonce $annonce ,ImagesRepository $imagesRepository): Response
    {
        $images = $imagesRepository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('studio/show.html.twig', [
            'annonce' => $annonce,
            'id' => 7,
            'images'=> $images,

        ]);
    }
    /**
     * @Route("/afficher/studio/{id}", name="studio_afficher_one" )
     */
    public function afficherOne(Annonce $annonce, ImagesRepository $repository, AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce'=>$annonce]);
        $avis =$avisRepository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('studio/afficherVente.html.twig', [
            'studio' =>  $annonce,
            'images' =>  $images,
            'avis' => $avis,

        ]);
    }
    /**
     * @Route("/location/studio/{id}", name="studio_afficher_location" )
     */
    public function afficherLocation(Annonce $annonce, ImagesRepository $repository , AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce'=>$annonce]);
        $avis = $avisRepository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('studio/afficherLocation.html.twig', [
            'studio' =>  $annonce,
            'images' =>  $images,
            'avis' => $avis,
        ]);
    }
    /**
     * @Route("/message/envoyer/studio/{action}/{id]", name="send_msg_studio")
     */
    public function sendMessage($action,Annonce $annonce, Request $request, MessageRepository $messageRepository, ImagesRepository $repository , AvisRepository $avisRepository)
    {
        $images = $repository->findBy(['idAnnonce' => $annonce]);
        $avis = $avisRepository->findBy(['idAnnonce' => $annonce]);
        $msg = new Message();
        $msg->setMsg($request->request->get('message'))
            ->setDataTime(new \DateTime())
            ->setEnvoyerPar("User")
            ->setUser($this->getUser())
            ->setRecuPar("Admin")
            ->setIdAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($msg);
        $this->getDoctrine()->getManager()->flush();
        //dump($messageRepository->findAll());exit;

        if ($action == "acheter") {

            return $this->render('studio/afficherVente.html.twig', [
                'images' => $images,
                'studios' => $annonce,
                'avis' => $avis,
                'message'=>'message envoyé',
            ]);
        } else {
            return $this->render('studio/afficherLocation.html.twig', [
                'images' => $images,
                'studio' => $annonce,
                'avis' => $avis,
                'message'=>'message envoyé',
            ]);
        }

    }
    /**
     * @Route("/commenter/studio/{id}", name="commenter_studio")
     */
    public function CommenterMaison(Annonce $annonce, Request $request)
    {
        //dump($request->request->all());exit;

        $avis = new Avis();
        $avis->setCommentaire($request->request->get('commentaire'))
            ->setdate(new\DateTime())
            ->setIduser($this->getUser())
            ->setidAnnonce($annonce);

        $this->getDoctrine()->getManager()->persist($avis);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('local_afficher_one',['id'=>$annonce->getId()]);
    }
}
